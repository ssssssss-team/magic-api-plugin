package org.d8s.magicapi.pojo;

/**
 * 对接方信息
 * @author wangshuai@e6yun.com
 * @date 5/24/2021 2:59 PM
 **/
public class OpenAuthAccountInfo {
    private String appId;
    private String appSecret;
    private String systemName;
    private String createTime;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAppSecret() {
        return appSecret;
    }

    public void setAppSecret(String appSecret) {
        this.appSecret = appSecret;
    }

    public String getSystemName() {
        return systemName;
    }

    public void setSystemName(String systemName) {
        this.systemName = systemName;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }
}
